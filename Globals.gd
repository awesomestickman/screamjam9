extends Node

# Variables ------------------------------------------
# State
var state = ""

# Day
var day = 1
var cycle = 3
var days_remaining = 2

# Player Money
var money = 500
var rent = 500
var payment = 0
var shop_item = 0

# Random Food Effects
var zombie = 0
var acid = 0
var vampire = 0
var schizo = 0


# Audio
var player = AudioStreamPlayer.new()
var sound = AudioStreamPlayer.new()

# Methods -------------------------------------------
func _process(delta):
	state = get_tree().get_current_scene().get_name()



# State ----------------
func _get_state():
	return state

# Audio ----------------
func _set_audio(name):
	self.add_child(player)
	player.stream = load(name)
	player.play()

func _play_sound(name):
	self.add_child(sound)
	sound.stream = load(name)
	sound.play()

# Day ------------------
func _get_day():
	return day

func _set_day(new):
	day = new

func _next_day():
	day += 1
	days_remaining -= 1

func _get_cycle():
	return cycle

func _get_days_remaining():
	return days_remaining

func _set_days_remaining(amount):
	days_remaining = amount

# Money ----------------
func _get_money():
	return money

func _set_money(amount):
	money = amount

func _add_money(amount):
	money += amount

func _sub_money(amount):
	money -= amount

func _get_rent():
	return rent

func _set_rent(amount):
	rent = amount

func _sub_rent(amount):
	rent -= amount

# Rent Menu ------------
func _get_payment():
	return payment

func _set_payment(amount):
	payment = amount

# Shop Menu ------------
func _get_shop_item():
	return shop_item

func _set_shop_item(amount):
	shop_item = amount

# Zombie ---------------
func _get_zombie():
	return zombie

func _set_zombie(amount):
	zombie = amount

func _add_zombie(amount):
	zombie += amount

func _sub_zombie(amount):
	zombie -= amount

# Acid -----------------
func _get_acid():
	return acid
	
func _set_acid(amount):
	acid = amount

func _add_acid(amount):
	acid += amount

func _sub_acid(amount):
	acid -= amount

# Vampire --------------
func _get_vampire():
	return vampire

func _set_vampire(amount):
	vampire = amount

func _add_vampire(amount):
	vampire += amount

func _sub_vampire(amount):
	vampire -= amount

# Schizo ---------------
func _get_schizo():
	return schizo

func _set_schizo(amount):
	schizo = amount

func _add_schizo(amount):
	schizo += amount

func _sub_schizo(amount):
	schizo -= amount
