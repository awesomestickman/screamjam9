extends Button

export(String) var type_of_food

#how high does food go when select
const YOFF = 25
#how fast food goes when select
const YSPEED = 2

#current text food is sending
var foodtext;

#reward money (and default reward)
var reward=2;

#get this so hovering knows where to fall to
var initialPos;
#used for hovering
var yoffset=0;
#is raising or falling
var falling = true

func eat_food():
	Globals._add_money(reward)
	get_parent().remove_child(self)

func eat_text():
	foodtext = "";
	if (type_of_food == "greencandy"):
		foodtext = "tastes like sour apples"
	elif (type_of_food == "pinkcandy"):
		foodtext = "tastes disappointingly like cherry"
	elif (type_of_food == "yellowcandy"):
		foodtext = "yecch! tastes like dried mold. What the heck is in this stuff?"
	return foodtext;
	
func hover_text():
	foodtext = "";
	if (type_of_food == "greencandy"):
		foodtext = "a green candy"
	elif (type_of_food == "pinkcandy"):
		foodtext = "a pink candy, reminds you of strawberries"
	elif (type_of_food == "yellowcandy"):
		foodtext = "looks normal enough, but why is the reward 300?"
	return foodtext;
func raise_food():
	falling = false
func lower_food():
	falling=true

#also sets food prices here
func _ready():
	initialPos = self.rect_global_position
	if (type_of_food == "pinkcandy"):
		reward = 50
	elif (type_of_food == "yellowcandy"):
		reward = 300

func _process(delta):
	if yoffset>0 && falling==true:
		yoffset = yoffset-YSPEED
	if yoffset<YOFF && falling==false:
		yoffset = yoffset+YSPEED
	self.rect_global_position.y = self.initialPos.y - yoffset
