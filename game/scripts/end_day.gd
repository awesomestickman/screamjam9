# end_day.gd
extends Node

# Rent Button
func _on_RentButton2_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene('res://game/submenus/RentMenu.tscn')

# Shop Button
func _on_ShopButton2_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene('res://game/submenus/ShopMenu.tscn')

# Next Day Button
func _on_Button_pressed():

	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	$FadeIn.show()
	$FadeIn.fade_in()

func _on_FadeIn_fade_finished():
	if (Globals._get_day() == 6):
		# if Rent Paid & Good Stats --> Good Ending
		get_tree().change_scene('res://game/GoodEnding.tscn')
		
		# if Rent Paid & Bad Stats --> Bad Ending
	
	# On Night Rent IS Due
	if (fmod(Globals._get_day(), Globals._get_cycle()) == 0): 

		# If Rent Unpaid --> Game Over
		if (Globals._get_rent() > 0):
			get_tree().change_scene('res://game/BadEnding.tscn')
		
		# If Rent Paid --> Reset Cycle, Next day
		elif (Globals._get_rent() <= 0):
			Globals._next_day()
			Globals._set_days_remaining(2)
			Globals._set_rent(500)
			get_tree().change_scene('res://game/DayPhaseIntro.tscn')
		

		# Good Ending
		#elif (Globals._get_rent() <= 0):
		#	get_tree().change_scene('res://game/GoodEnding.tscn')
	
	# On Night Rent IS NOT Due
	else: 

		for contract in get_node("/root/DevTools/Contracts").get_children():
			contract._decrease_day()
			DevTools._check_contracts()

		Globals._next_day()
		get_tree().change_scene('res://game/DayPhaseIntro.tscn')
