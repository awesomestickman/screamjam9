# game_start.gd
extends Node

# Reset/Initialize Variables during Game Start/Intro Scene
# in case of multiple playthroughs in a single play session
func _ready():
	Globals._set_day(1)
	Globals._set_days_remaining(2)
	Globals._set_rent(500)
	Globals._set_money(500)
	Globals._set_zombie(0)
	Globals._set_acid(0)
	Globals._set_vampire(0)
	Globals._set_schizo(0)
	
	Globals._set_audio("res://assets/audio/Music/Intro.wav")

func _on_Button_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene('res://game/DayPhaseIntro.tscn')
