extends Node

export(String, FILE, "*.tscn") var next_scene

func _ready():
	Globals._set_audio("res://assets/audio/Music/Home.wav")

func _on_Button_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene(next_scene)
