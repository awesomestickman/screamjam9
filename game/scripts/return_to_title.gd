extends Control

func _ready():
	Globals._set_audio("res://assets/audio/Music/Home_Luxury.wav")

func _on_Button_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene('res://title_screen/TitleScreen.tscn')
