extends Node

func _ready():
	# Update Day # Label
	$CenterContainer/VBoxContainer/Day.text = "Day " + str(Globals._get_day())
	
	# Update Rent Due in # Days Label
	if (Globals._get_days_remaining() == 0):
		$CenterContainer/VBoxContainer/Due.text = "Rent Due Today"
	else:
		$CenterContainer/VBoxContainer/Due.text = "Rent Due in " + str(Globals._get_days_remaining()) + " Days"
	
	# Update Flavor Text
	# Default
	$CenterContainer/VBoxContainer/Flavor.text = "Back to the old grind."
	
	# Check Status Variables
	if (Globals._get_zombie() >= 5):
		$CenterContainer/VBoxContainer/Flavor.text = "Walked by an old lady on the way to work today. She smelled delicious."
	if (Globals._get_acid() >= 5):
		$CenterContainer/VBoxContainer/Flavor.text = "The cars sound really brown today."
	if (Globals._get_vampire() >= 5):
		$CenterContainer/VBoxContainer/Flavor.text = "The sun is so BRIGHT! I'll just walk in the shadows."
	if (Globals._get_schizo() >= 5):
		$CenterContainer/VBoxContainer/Flavor.text = "I can't even remember getting out of bed today..."
	
	$CenterContainer/VBoxContainer/Flavor.ALIGN_CENTER
	
	Globals._set_audio("res://assets/audio/Music/Main.wav")

# Start Work Button
func _on_Button_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene("res://table/Table.tscn")
