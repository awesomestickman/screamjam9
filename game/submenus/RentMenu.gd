extends Control

func _ready():
	Globals._set_payment(0)

func _physics_process(delta):
	$VBoxContainer/HBoxContainer/RightColumn/RentNum.text = "$" + str(Globals._get_rent())
	$VBoxContainer/HBoxContainer/RightColumn/WalletNum.text = "$" + str(Globals._get_money())
	$VBoxContainer/HBoxContainer/RightColumn/PaymentNum.text = "$" + str(Globals._get_payment())

func _on_Increase2_pressed():
	if (Globals._get_payment() < Globals._get_money()):
		Globals._play_sound("res://assets/audio/SFX/powerUp2.ogg")
		var temp = Globals._get_payment()
		Globals._set_payment(temp + 100)

func _on_Decrease2_pressed():
	if (Globals._get_payment() > 0):
		Globals._play_sound("res://assets/audio/SFX/twoTone2.ogg")
		var temp = Globals._get_payment()
		Globals._set_payment(temp - 100)

func _on_Pay2_pressed():
	if (Globals._get_payment() <= Globals._get_money()):
		Globals._play_sound("res://assets/audio/SFX/powerUp6.ogg")
		Globals._sub_rent(Globals._get_payment())
		Globals._sub_money(Globals._get_payment())
		Globals._set_payment(0)
	else:
		Globals._play_sound("res://assets/audio/SFX/powerUp6.ogg")

func _on_CloseMenu_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene('res://game/NightPhase.tscn')
