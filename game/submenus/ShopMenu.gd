extends Control

func _ready():
	Globals._set_shop_item(0)

func _process(delta):
	$VBoxContainer.ALIGN_CENTER
	$VBoxContainer/Information/RightColumn/PriceNum.text = "$" + str(Globals._get_shop_item())
	$VBoxContainer/Information/RightColumn/WalletNum.text = "$" + str(Globals._get_money())

func _on_Item1_pressed():
	Globals._play_sound("res://assets/audio/SFX/powerUp2.ogg")
	Globals._set_shop_item(10)

func _on_Item2_pressed():
	Globals._play_sound("res://assets/audio/SFX/powerUp2.ogg")
	Globals._set_shop_item(20)

func _on_Item3_pressed():
	Globals._play_sound("res://assets/audio/SFX/powerUp2.ogg")
	Globals._set_shop_item(30)

func _on_Buy_pressed():
	if (Globals._get_shop_item() <= Globals._get_money()):
		Globals._play_sound("res://assets/audio/SFX/twoTone2.ogg")
		Globals._sub_money(Globals._get_shop_item())
	else:
		Globals._play_sound("res://assets/audio/SFX/powerUp6.ogg")

func _on_CloseMenu_pressed():
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	get_tree().change_scene('res://game/NightPhase.tscn')
