extends Control


var foodtype = ""
var foodleft = 0
var daysleft = 0
var reward = 0

func _set_up(food,fleft,dleft,money):
	foodtype = food
	foodleft=fleft
	daysleft=dleft
	reward=money
	$VBoxContainer/foodtype.text = food
	$VBoxContainer/foodleft.text = str(foodleft) + " food left"
	$VBoxContainer/daysleft.text = str(daysleft) + " days left"
	$VBoxContainer/reward.text = "$" + str(reward)

func _decrease_food():
	foodleft = foodleft - 1
	$VBoxContainer/foodleft.text = str(foodleft)+ " food left"
	
func _decrease_day():
	daysleft = daysleft - 1
	$VBoxContainer/daysleft.text = str(daysleft)+ " days left"



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
