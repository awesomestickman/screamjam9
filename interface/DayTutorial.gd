# DayTutorial.gd
extends Control

func _on_Help_pressed():
	
	$ColorRect.visible = not $ColorRect.visible
	$Instructions.visible = not $Instructions.visible
	
	if ($ColorRect.visible == true):
		Globals._play_sound("res://assets/audio/SFX/powerUp2.ogg")
		$Help.text = "Close"
		get_tree().get_root().get_node(str(Globals._get_state()) + "/CanvasLayer").layer = 20
	else:
		Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
		$Help.text = "Help"
		get_tree().get_root().get_node(str(Globals._get_state()) + "/CanvasLayer").layer = 1
