
extends Node2D

# Contracts !!! ---------------------

#TODO - add check contract with food input to see if food needs to be incremented

#seeing if contracts need to be rolled over
func _check_contracts():
	for contract in $Contracts.get_children():
		if contract.daysleft <= 0:
			$Contracts.remove_child(contract)
			_add_contract()
	
#everything to do with contract generation and choosing stats
func _add_contract():
	#TODO possibly add day as input so gives contracts relating to stage 3?
	var newContract = preload("res://interface/Contract.tscn")
	var instancedContract = newContract.instance()
	instancedContract._set_up("yellowcandy",2,2,200)

	
	$Contracts.add_child(instancedContract)
	


func _ready():
	_add_contract()
	_add_contract()
	_add_contract()



func _process(delta):
	$VBoxContainer/Scene.text = "Scene: " + Globals._get_state()
	$VBoxContainer/Day.text = "Day: " + str(Globals._get_day())
	$VBoxContainer/RentDue.text = "Rent due in: " + str(Globals._get_days_remaining()) + " days"
	$VBoxContainer/Rent.text = "Rent: " + str(Globals._get_rent())
	$VBoxContainer/Money.text = "Money: " + str(Globals._get_money())
	$VBoxContainer/Zombie.text = "Zombie: " + str(Globals._get_zombie())
	$VBoxContainer/Acid.text = "Acid: " + str(Globals._get_acid())
	$VBoxContainer/Vampire.text = "Vampire: " + str(Globals._get_vampire())
	$VBoxContainer/Schizo.text = "Schizo: " + str(Globals._get_schizo())

func _input(event):
	if event.is_action_pressed("devmode"):
		$ColorRect.visible = not $ColorRect.visible
		$VBoxContainer.visible = not $VBoxContainer.visible
		
