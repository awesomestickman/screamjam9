extends Control

#this script is for allowing the table to 
#communicate with the food on the table.

#this variable ensures eating text sticks around for a second 
const FOODTIME = 20
var foodtimer = 0

func _ready():
	for button in $VBox/centerRow.get_children():
		button.connect("mouse_entered", self, "_on_mouse_hover", [button])
		button.connect("mouse_exited", self, "_on_mouse_leave", [button])
		button.connect("pressed", self, "_on_Button_pressed", [button])

func _on_Button_pressed(button):
	$StatusLabel.text = button.eat_text()
	button.eat_food()
	foodtimer=FOODTIME
	
func _on_mouse_hover(button):
	if(foodtimer<=0):
		$StatusLabel.text = button.hover_text() + "\nReward: " + str(button.reward)
	button.raise_food()

func _on_mouse_leave(button):
	if($StatusLabel.text == (button.hover_text() + "\nReward: " + str(button.reward))):
		$StatusLabel.text = ""
	button.lower_food()

func _process(delta):
	foodtimer = foodtimer-1


