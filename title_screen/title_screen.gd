extends Control

var scene_path_to_load

func _ready():

	Globals._set_audio("res://assets/audio/Music/Main.wav")
	$Menu/Buttons/NewGameButton.grab_focus()
	for button in $Menu/Buttons.get_children():
		button.connect("pressed", self, "_on_Button_pressed", [button.scene_to_load])
		


func _on_Button_pressed(scene_to_load):
	Globals._play_sound("res://assets/audio/SFX/tone1.ogg")
	$FadeIn.show()
	$FadeIn.fade_in()
	scene_path_to_load = scene_to_load

func _on_FadeIn_fade_finished():
	$FadeIn.hide()
	get_tree().change_scene(scene_path_to_load)



